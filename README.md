# MongoDB <a name="inicio"/>

## Contenido
* 1. [Introducción](#Introducción)
* 2. [Características](#Características)
* 3. [Ecosistema](#Ecosistema)
* 4. [MongoDB Atlas](#MongoDB-Atlas)
* 5. [Bases de Datos NoSQL](#Bases-de-Datos-NoSQL)
* 6. [Comandos básicos y CRUD](#Comandos)
    * 1. [CREATE](#CREATE)
    * 2. [READ](#READ)
    * 3. [UPDATE](#UPDATE)
    * 4. [DELETE](#DELETE)
    * 5. [Índices](#Índices)


---
## Introducción <a name="Introducción"/> [↑](#inicio)
Es una base de datos no relacional basada en documentos que nos permite guardarlos de forma distribuida. Ofrece una gran escalabilidad y flexibilidad, y un modelo de consultas e indexación avanzado.

**Bases de datos:**
* Contenedor físico de colecciones.
* Cada base de datos tiene su archivo propio en el sistema de archivos.
* Un cluster puede tener múltiples bases de datos.

**Colecciones:**
* Agrupación de documentos.
* Equivalente a una tabla en las bases de datos relacionales.
* No impone un esquema.

**Documentos:**
* Un registro dentro de una colección.
* Es análogo a un objeto JSON (BSON (*Representación binaria de un JSON*)).
* Unidad básica dentro de MongoDB.

![alt text](./images/SQL-MongoDB_Correspondence.PNG "SQL-MongoDB Correspondence")

**BSON.** Formato de datos que, en esencia, son ficheros JSON binarizados, esto es, no legibles directamente en formato texto pero que sirve a tecnologías, como las bases de datos, para acceder de una manera más dirigida y directa a la información. Soporta más tipos de datos a diferencia de un JSON, como fechas, valores binarios, etc.

**Esquemas:** Son la forma en que organizamos nuestros documentos en nuestras colecciones. MongoDB no impone ningún esquema pero podemos seguir buenas prácticas y estructurar nuestros documentos de forma parecida (no igual) para aprovechar la flexibilidad y escalabilidad de la base de datos sin aumentar la complejidad de nuestras aplicaciones.

**Relaciones:** Son la forma en que nuestras entidades o documentos se encuentran enlazados unos con otros. Por ejemplo: Una carrera tiene multiples cursos y cada curso tiene multiples clases.


---
## Características <a name="Características"/> [↑](#inicio)
* Permite guardar documentos en estructuras de forma **BSON**, permitiendo así una gran flexibilidad al momento de modelar situaciones de la vida real.
* Es una base de datos distribuida, es decir la información puede ser almacenada en varios servidores (conocido como: *Cluster de Mongo DB*), permitiendo así la escala de forma horizontal, agregando nuevos servidores.
* Es schema less, permite que los documentos se puedan guardar sin una estructura definida y limitada.
* El lenguaje para realizar *queries*, índices y agregaciones, es muy expresivo y mediante funciones JavaScript. Las agregaciones facilitan la extracción de información de múltiples colecciones y documentos.
* Es de código abierto, y debido a esto muchas compañias se encuentran detrás del desarrollo de esta plataforma, por ejemplo **MongoDB Atlas** que es un cluster de Mongo como servicio, **mLab, MongoCLUSTERS, aws**, etc. 
* Los documentos no pueden ser mayores a 16 MB.
* One to one: Documentos embebidos
* One to many: Documentos embebidos cuando la información no va a cambiar frecuentemente y referencias cuando si.


---
## Ecosistema <a name="Ecosistema"/> [↑](#inicio)
* **MongoDB Server.\*** Es donde se almacenan los documentos (los datos).
* **Versiones\*:** 
    * *Community\**, de código abierto, se puede descargar y ver como esta implementado el motor de Bases de datos por debajo, 
    * *Enterprise*, versión bajo licencia vendida por los desarrolladores de Mongo, suele incluir caracterísitcas extras y 
    * *Atlas (Cloud)\**, es un Mongo como servicio, permite tener clusters listos para producción en un par de clicks.
* **MongoDB Shell.\*** Consola de Mongo, es el medio para interactuar con el servidor y administrarlo.
* **MongoDB Compass.\*** Interfaz gráfica para hacer *queries* a servidores de MongoDB.
* **Conectores.\*** Librerías agregadas a los proyectos para permitir la comunicación con MongoDB.
* **MongoDB Mobile.** Permite hacer *queries* desde dispositivos mobiles.
* **Stitch.** Permite crear funciones serverless sobre la Base de Datos y permite agregar lógica explicita. (Producto incluido en el entorno de Atlas)
* **MongoDB Charts.** Conectores para hacer Business Intelligence.

\* Se verán durante el curso.


---
## MongoDB Atlas <a name="MongoDB-Atlas"/> [↑](#inicio)
### Características
* Aprovisionamiento automático de clusters con MongoDB.
* Alta disponibilidad. 
* Altamente escalable.
* Seguro.
* Disponible en AWS, GCP y Microsoft Azure.
* Fácil monitoreo y optimización.

### Nuevo Cluster
**Nombre:** CursodePlatzi

**URL:** https://cloud.mongodb.com/v2/5ffe53a7b4e139287d2dc8ef#clusters

**Para realizar la conexión desde consola (cmd):**
* Desde la consola (cmd) moverse a la ubicación 'C:\Program Files\MongoDB\Server\4.4\bin' `cd C:\Program Files\MongoDB\Server\4.4\bin`
* Correr el comando `mongo "mongodb+srv://cursodeplatzi.xhmpk.mongodb.net/" --username platzi-admin` (url de conexión)

**NOTAS:**
* Si hay problemas durante la conexión a la Base de Datos, generalmente el problema es porque el proveedor de Internet cambio la IP.
* Cree el usuario en MongoDB Atlas con mis credenciales de Google. 
* Credenciales del cluster: platzi-admin | JLw0rFda01Sw0MYE

### Drivers
Los drivers de MongoDB son librerías oficiales o desarrolladas por la comunidad que podemos usar para comunicar nuestras aplicaciones con las bases de datos. Sin nuestros drivers no podemos trabajar con nuestros cluster de base de datos. Una de las más populares es Mongoid, un ORM que convierte nuestros código Ruby en *queries* que entiende nuestra base de datos.

### Instalación de drivers en diferentes lenguajes
* **Python:** `python -m pip install pymongo`
* **Node.js:** `npm install mongodb --save`
* **Ruby:** `gem install mongoid`
* **GO:** `dep ensure -add go.mongodb.org/mongo-driver/mongo`
* **Java:** Utilizando algún gestor de dependias como *Maven* o *Gradle*.

### Recomendaciones de Arquitectura y Paso a Producción
* Usar proveedores cloud con alta disponibilidad: AWS, Google Cloud o Azure son muy buenas opciones.
* No complicarse pensando en administración de servidores con MongoDB, servicios como MongoDB Atlas o mlab son muy buenas opciones.
* Guardar las credenciales en variables de entorno o archivos de configuración fuera del proyecto.
* Asegurarse que tu cluster se encuentra en la misma región del proveedor que tu aplicación.
* Hacer *VPC peering* entre la VPC de tu aplicación y la VPC de tu cluster.
* Cuidar la lista de IPs blancas.
* Considerar habilitar la autenticación en dos pasos.
* Actualizar constantemente la versión de MongoDB.
* Separar los ambientes de desarrollo, test y producción.
* Habilitar la opción de almacenamiento encriptado.

### DOCS
* **BSON:** https://docs.mongodb.com/manual/reference/bson-types/
* **MongoDB Workbook:** http://nicholasjohnson.com/mongo/course/workbook/

#### Buenas Practicas.
* https://geekflare.com/mongodb-sharding-best-practices/
* https://www.infoq.com/articles/Starting-With-MongoDB/
* https://www.mongodb.com/blog/post/time-series-data-and-mongodb-part-2-schema-design-best-practices
* https://www.mongodb.com/blog/post/performance-best-practices-mongodb-data-modeling-and-memory-sizing


---
## Bases de Datos NoSQL <a name="Bases-de-Datos-NoSQL"/> [↑](#inicio)
Dentro de las Bases de Datos (DBs) NoSQL se tiene cuatro grandes familias según el modelo de datos:
* **Key-value stores (DBs basadas en datos, clave-valor)**
* **Graph Databases (DBs basadas en Grafos)**
* **Wide-column stores (DBs Columnares)** 
* **Document Databases (DBs basadas en documentos)**

### Key-value stores
Poseen la estructura simple de llave-valor, en la cual se almacenan los datos; son bastante utilizadas para guardar información en cache, información de sesión de usuarios. Son bastante rápidas al momento de consultar pero la simplicidad de su estructura hace que el modelado de casos reales no sea conveniente. **Redis** es un motor de DBs que destaca en este modelo.

### Graph Databases
DBs basadas en grafos. Nos permiten establecer relaciones dentro de las entidades, para consultas más eficientes que en las DB relacionales por ejemplo un Tweet tiene relación con usuarios, con links, etc. En la industria algunas de las mas utilizadas son **Neo4j** o **JanusGraph**

### Wide-column stores
DBs columnares, su característica principal es que poseen dos llaves, una llave de fila y otra llave de columnas, eso permite hacer *queries* mucho más rápidas (buen rendimiento), permiten guardar grandes cantidades de información, son principalmente utilizadas en Big Data, IoT, Sistemas de Recomendación, etc. Su incoveniente es al momento de modelar los datos, ya que suele ser complicado. Dentro de la industria, **Apache Casandra** es basatante utilizada.

### Document Databases
DBs basadas en documentos, como lo es **MongoDB**, permiten guardar documentos, en forma de estructuras tipo JSON, dentro de colecciones, esta flexibilidad permite modelar casos de la vida real de manera muy sencilla, además poseen un buen performance debido a su simplicidad.

### DOCS
* **Mongo vs SQL:** https://platzi.com/blog/mongo-vs-sql/


---
## Comandos básicos y CRUD <a name="Comandos"/> [↑](#inicio)
Comando | Descripción
--------|------------
`show dbs` | Muestra las Bases de Datos (DBs) en el cluster
`use 'dbName'` | Crea una DB y si ya existe una, solo la selecciona para utilizarla. **Mongo no crea DBs vacias, la nueva DB no se guarda hasta que no se crea un documento en ella.**
`show collections` | Muestra las colecciones de la DB
`db.'collectionName'.help()` |  Retorna una lista de comandos que pueden ser utilizados en la colección

### CREATE <a name="CREATE"/> [↑](#inicio)
Comandos para hacer inserción de documentos en las colecciones.

#### Insertar un documento (insertOne())
* `db.'collectionName'.insertOne({...})` => Inserta un documento dentro de la colección seleccionada. Si no existe la colección, Mongo la crea. Entre parentesis se ingresa el documento en formato JSON. **MongoDB solo guarda documentos con IDs únicas.**

#### Insertar un arreglo de documentos (insertMany())
* `db.'collectionName'.insertMany([{...}, {...}])` => Inserta multiples documentos en la colección seleccionada, recibe un arreglo de documentos. **MongoDB posee *atomicidad* a nivel de los documentos, es decir, durante las opreaciones de escritura múltiple MongoDB garantiza que si ocurre un error durante el registro de alguno de los documentos, este documento hacen un *rollback*, "eliminando" así su registro o no guardadolo.**

**NOTAS:**
* El campo _id si no es agregado por nosotros de forma explícita, MongoDB lo agrega como un ObjectId.
* El campo _id es obligatorio en todos los documentos.
* **Atomicidad**, todas las operaciones de escritura en MongoDB son atómicas a nivel de documentos.

### READ <a name="READ"/> [↑](#inicio)
Permite buscar documentos dentro de las colecciones.

#### Busqueda por igualdad
* `db.'collectionName'.find('condition')` => Retorna los documentos que cumplen con la condición agregada. **Se puede agregar el metodo `pretty()` para visualizar la respuesta de una mejor manera. O el método `count()` para retornar la cantidad de documentos que coinciden con la condición** Ejemplo: `db.inventory.find({item: "canvas"}).pretty()`
* `db.'collectionName'.findOne()` => Retorna el primer documento de la colección seleccionada que cumpla con el filtro de acuerdo al orden natural en que los documentos se encuentren guardados en disco. El **orden natural** es la estructura como se guardan los documentos dentro del archivo, esa estructura se puede definir de acuerdo a los índices. Este comando también permite agregar una condición de busqueda. Ejemplo: `db.inventory.findOne({_id: ObjectId("6008f860b2689ed4bbefced2")})`

#### Operadores
Permiten crear diferentes tipos de condiciones. En MongoDB se denotan por el simbolo de dollar '$' algunos de los operadores existentes son:

Operador | Descripción
---------|-------------
`$eq` | IGUAL (Equal) .
`$ne` | DIFERENTE (No Equal) .
`$gt` | MAYOR A (Greater Than) .
`$gte` | MAYOR A O IGUAL A (Greater Than or Equal) .
`$lt` | MENOR A (Less Than) .
`$lte` | MENOR A O IGUAL A (Less Than or Equal) .
`$in` | Valores dentro de un rango.
`$nin` | Valores fuera de un rango.
`$and` | Une *queries* con un AND lógico. Selecciona los documentos que cumplen TODAS las condiciones agregadas.
`$not` | Invierte el efecto de un query.
`$nor` | Une *queries* con un NOR lógico. Selecciona los documentos que NO cumplen NINGUNA de las condiciones agregadas.
`$or` | Une *queries* con un OR lógico. Selecciona los documentos que cumplen AL MENOS UNA de las condiciones agregadas.
`$exist` | Documentos que cuentan con un campo específico.
`$type` | Documentos que cuentan con un campo de un tipo específico (String, Boolean, etc.).
`$all` | Todos los elementos del arreglo.
`$elemMatch` | Documentos que cumplen la condición del `$elemMatch` en uno de sus elementos. Permite escribir una query dentro de objetos (como elementos) de un documento.
`$size` | Documentos que cumplen con el tamaño del arreglo agregado.

#### Proyecciones
Permite traer solamente los valores de los elementos específicados para el documento que cumpla con el filtro. `db.'collectionName'.findOne('condition', {Elemento1: 1, Elemento2: 1, ElementoN: 1})`. En el listado de elementos a retornar, el '1' indica que el valor de ese elemento es requerido, mientras que el '0' indica que será ignorado. El _id siempre se retornará, a menos que se indique lo contrario explicitamente. Ejemplo: `db.inventory.findOne({status: "A"}, {item: 1, status: 1})`

### UPDATE <a name="UPDATE"/> [↑](#inicio)
Comandos para actualizar documentos en las colecciones.

#### Actualiza un documento (updateOne() o update())
* `db.'collectionName'.updateOne({'Filtro del documento a actualizar', 'Valores a actualizar'})` => Actualiza el primer documento del orden natural dentro en la colección seleccionada que cumpla con el filtro. Ejemplo: `db.inventory.updateOne({ item: "paper" }, { $set: { "size.uom": "cm", status: "P" }, $currentDate: { lastModified: true }})`

#### Actualiza un arreglo de documentos (updateMany())
* `db.'collectionName'.updateMany({'Filtro de los documentos a actualizar', 'Valores a actualizar'})` => Actualiza todos los documentos de la colección seleccionada, que cumplan con el filtro. Ejemplo: `db.inventory.updateMany({ "qty": { $lt: 50 } }, { $set: { "size.uom": "in", status: "P" }, $currentDate: { lastModified: true }})`

#### Reemplaza un documento.
* `db.'collectionName'.replaceOne({'Filtro del documento a remplazar', 'Valores a remplazar'})` => Reemplaza un documento y conserva su _id. Ejemplo: `db.inventory.replaceOne({ item: "paper" }, { item: "paper", instock: [ { warehouse: "A", qty: 60 }, { warehouse: "B", qty: 40 } ] })`

#### Operadores
Cumplen funciones específicas. En MongoDB se denotan por el simbolo de dollar '$' algunos de los operadores existentes son:

Operador | Descripción
---------|-------------
`$eq` | Remplaza los valores de los campos de un documento por el valor especificado en el comando.
`$unset` | Borra un campo particular de un documento.
`$push` | Agrega un campo a un arreglo de un documento. *
`$each` | Agrega varios campos a un arreglo de un documento. *
`$addToSet` | Agrega un campo único a un arreglo de un documento, si el campo ya existe en el arreglo entonces no lo crea. *
`$pop` | Elimina un campo (elemento) de un arreglo de un documento. **NOTA:** Si se agrega un "1" se elimina el último elemento del array, si se agrega un "-1" se elimina el primer elemento.
`$pull` | Elimina (un) campo(s) especificado(s) de un arreglo de un documento.
`$pullAll` |  Elimina todos los elementos del valor que se le pasa al arreglo del documento especificado.
`$elemMatch` | Agrega un campo a un objeto de un arreglo de objetos. El comando $elemMatch permite identificar un objeto con los campos que conocemos.
`$inc` | Incrementa el valor de un campo por el número especificado.

**\*** Si se agrega un arreglo que no existe en el documento entonces se crea un nuevo array.

### DELETE <a name="DELETE"/> [↑](#inicio)
Comandos para eliminar documentos de las colecciones.

#### Elimina un documento (deleteOne())
* `db.'collectionName'.deleteOne({'Filtro del documento a eliminar'})` => Elimina el primer documento del orden natural y en cumplimiento con el filtro de la colección seleccionada. Ejemplo: `db.inventory.deleteOne({ status: "P" })`

#### Elimina un arreglo de documentos (deleteMany())
* `db.'collectionName'.deleteMany({'Filtro de los documentos a eliminar'})` => Elimina todos los documentos de la colección seleccionada, que cumplan con el filtro. Ejemplo: `db.inventory.deleteMany({ status: "P" })`
* `db.'collectionName'.deleteMany({})` => Elimina todos los documentos de la colección.

### Índices <a name="Índices"/> [↑](#inicio)
Los índices nos ayudan a que nuestras consultas sean más rápidas ya que sin ellos MongoDB debería escanear toda la colección en busca de los resultados.
Tipos de índices:
* De un solo campo
* Compuestos
* Multi-llave
* Geoespaciales
* De texto
* Hashed

**Comandos**
* `db.'collectionName'.getIndexes()` => Devuelve los índices de la colección agregada.
* `db.'collectionName'.createIndex({...})` => Agrega un nuevo índice a la colección. Ejemplo: `db.cursos.createIndex({nombre: "text"})`

#### Busqueda aplicando índices
* `db.cursos.find({$text: {$search: "aws"}}, {nombre: 1})` => Busqueda aplicando índices y proyecciones, para traer solamente el nombre de los cursos que coinciden con el índice agregado

### DOCS
* **Operadores:** https://docs.mongodb.com/manual/reference/operator/
* **SQL to Aggregation Mapping Chart:** https://docs.mongodb.com/manual/reference/sql-aggregation-comparison/
* **Agregaciones:** https://docs.mongodb.com/manual/aggregation/